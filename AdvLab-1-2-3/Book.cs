﻿namespace AdvLab_1_2_3
{
    class Book
    {
        public static int MaxBookName = "Название".Length;
        public static int MaxAuthorName = "Автор".Length;

        public readonly int Id;
        public readonly string Name;
        public readonly string Author;
        public readonly int YearOfPublication;

        public int Section { get; set; }
        public int Shelf { get; set; }
        public bool IsAccessible { get; set; }

        public Book(int id, string author, string name, int yearOfPublication, int section, int shelf, bool isAccessible)
        {
            Id = id;
            Author = author;
            Name = name;
            YearOfPublication = yearOfPublication;
            Section = section;
            Shelf = shelf;
            IsAccessible = isAccessible;

            if (MaxBookName < Name.Length) MaxBookName = Name.Length;
            if (MaxAuthorName < Author.Length) MaxAuthorName = Author.Length;
        }
    }
}