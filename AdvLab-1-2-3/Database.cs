﻿using System;
using System.IO;
using System.Collections.Generic;

namespace AdvLab_1_2_3
{
    class Database
    {
        static void Main()
        {
            string[] books = File.ReadAllLines("../../../../Books.csv");
            string[] readers = File.ReadAllLines("../../../../Readers.csv");
            string[] booksAndReaders = File.ReadAllLines("../../../../BooksAndReaders.csv");

            Scheme bookScheme = Scheme.GetScheme("../../../../BookScheme.json");
            Scheme readerScheme = Scheme.GetScheme("../../../../ReaderScheme.json");
            Scheme bookAndReaderScheme = Scheme.GetScheme("../../../../BooksAndReadersScheme.json");

            DataExamination.IsAppropriateToSchema(books, bookScheme);
            DataExamination.IsRightCountOfColumns(books, bookScheme);
            DataExamination.IsAppropriateToSchema(readers, readerScheme);
            DataExamination.IsRightCountOfColumns(readers, readerScheme);

            EmptyDateToDefault(booksAndReaders);
            DataExamination.IsAppropriateToSchema(booksAndReaders, bookAndReaderScheme);
	        DataExamination.IsRightCountOfColumns(booksAndReaders, bookAndReaderScheme);

            List<Reader> readersData = DataReading.ReadTheReadersData(readers);
            List<Book> booksData = DataReading.ReadTheBooksData(books);
            List<BookAndReader> booksAndReadersData = DataReading.ReadTheBooksAndReadersData(booksAndReaders, booksData, readersData);

            DataWriting.OutputTheDatabase(booksAndReadersData, booksData);
        }

        static void EmptyDateToDefault(string[] booksAndReaders)
        {
            DateTime defaultDate = default;
            for (int i = 0; i < booksAndReaders.Length; i++)
            {
                string[] elements = booksAndReaders[i].Split(";");
                if (String.IsNullOrEmpty(elements[3]))
                    booksAndReaders[i] += defaultDate.ToString();
            }
        }
    }
}