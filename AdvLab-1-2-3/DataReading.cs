﻿using System;
using System.Collections.Generic;

namespace AdvLab_1_2_3
{
    class DataReading
    {
        public static List<Book> ReadTheBooksData(string[] books)
        {
            var booksData = new List<Book>();

            for (int i = 0; i < books.Length; i++)
            {
                string[] elements = books[i].Split(";");

                int id = Int32.Parse(elements[0]);
                string author = elements[1];
                string name = elements[2];
                int yearOfPublication = Int32.Parse(elements[3]);
                int section = Int32.Parse(elements[4]);
                int shelf = Int32.Parse(elements[5]);
                bool isAccessible = bool.Parse(elements[6]);

                booksData.Add(new Book(id, author, name, yearOfPublication, section, shelf, isAccessible));
            }

            booksData.Sort(delegate (Book book1, Book book2)
            { return book1.Id.CompareTo(book2.Id); });
            return booksData;
        }

        public static List<Reader> ReadTheReadersData(string[] readers)
        {
            var readersData = new List<Reader>();

            for (int i = 0; i < readers.Length; i++)
            {
                string[] elements = readers[i].Split(";");

                int id = Int32.Parse(elements[0]);
                string name = elements[1];

                readersData.Add(new Reader(id, name));
            }

            readersData.Sort(delegate (Reader reader1, Reader reader2)
            { return reader1.Id.CompareTo(reader2.Id); });
            return readersData;
        }

        public static List<BookAndReader> ReadTheBooksAndReadersData(string[] booksAndReaders, List<Book> books, List<Reader> readers)
        {
            var booksAndReadersData = new List<BookAndReader>();

            for (int i = 0; i < booksAndReaders.Length; i++)
            {
                string[] elements = booksAndReaders[i].Split(";");

                int bookId = Int32.Parse(elements[0]);
                int readerId = Int32.Parse(elements[1]);
                DateTime dateOfGetting = DateTime.Parse(elements[2]);

                var newNote = new BookAndReader(books[bookId - 1], readers[readerId - 1], dateOfGetting);
                if (DateTime.Parse(elements[3]) != default) newNote.SetTheDateOfReturning(DateTime.Parse(elements[3]));
                booksAndReadersData.Add(newNote);
            }

            booksAndReadersData.Sort(delegate (BookAndReader note1, BookAndReader note2)
            { return note1.Book.Id.CompareTo(note2.Book.Id); });
            return booksAndReadersData;
        }
    }
}