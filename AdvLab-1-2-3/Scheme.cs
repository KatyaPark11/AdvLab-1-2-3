﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace AdvLab_1_2_3
{
    class Scheme
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; private set; }

        [JsonProperty(PropertyName = "columns")]
        public List<ElementOfTheScheme> Elements = new();

        public static Scheme GetScheme(string path)
        {
            return JsonConvert.DeserializeObject<Scheme>(File.ReadAllText(path));
        }
    }
}