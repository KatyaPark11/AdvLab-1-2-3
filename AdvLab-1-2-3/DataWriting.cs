﻿using System;
using System.Text;
using System.Collections.Generic;

namespace AdvLab_1_2_3
{
    class DataWriting
    {
        public static void OutputTheDatabase(List<BookAndReader> booksAndReadersData, List<Book> booksData)
        {
            var table = new StringBuilder();
            List<BookAndReader> booksAndReaders = DeleteExtraNotes(booksAndReadersData);
            table.Append(CreateTheTableHead());

            int indexInBRList = 0;

            for (int i = 0; i < booksData.Count; i++)
            {
                if (booksData[i].IsAccessible)
                    table.Append(CreateTheLineOfBookData(booksData[i]));
                else
                    table.Append(CreateTheLineOfBRData(booksAndReaders[indexInBRList++]));
            }

            Console.WriteLine(table);
        }

        static List<BookAndReader> DeleteExtraNotes(List<BookAndReader> booksAndReaders)
        {
            var result = new List<BookAndReader>();

            foreach (var note in booksAndReaders)
            {
                if (note.DateOfReturning == default)
                {
                    result.Add(note);
                    if (BookAndReader.MaxReaderName < note.Reader.Name.Length) 
                        BookAndReader.MaxReaderName = note.Reader.Name.Length;
                }
            }

            return result;
        }

        static StringBuilder CreateTheLineOfBRData(BookAndReader bookAndReader)
        {
            var BRData = new List<string>();

            string author = bookAndReader.Book.Author.PadRight(Book.MaxAuthorName);
            BRData.Add(author);
            string name = bookAndReader.Book.Name.PadRight(Book.MaxBookName);
            BRData.Add(name);
            string reader = bookAndReader.Reader.Name.PadRight(BookAndReader.MaxReaderName);
            BRData.Add(reader);
            string dateOfGetting = bookAndReader.DateOfGetting;
            BRData.Add(dateOfGetting);

            return CreateTheLineOfElements(BRData);
        }

        static StringBuilder CreateTheLineOfBookData(Book book)
        {
            var bookData = new List<string>();

            string author = book.Author.PadRight(Book.MaxAuthorName);
            bookData.Add(author);
            string name = book.Name.PadRight(Book.MaxBookName);
            bookData.Add(name);
            string reader = new(' ', BookAndReader.MaxReaderName);
            bookData.Add(reader);
            string dateOfGetting = new(' ', BookAndReader.DateLength);
            bookData.Add(dateOfGetting);

            return CreateTheLineOfElements(bookData);
        }

        static StringBuilder CreateTheTableHead()
        {
            var separators = new List<string>();
            var headers = new List<string>();
            var tableHead = new StringBuilder();

            string author = "Автор".PadRight(Book.MaxAuthorName);
            headers.Add(author);
            string name = "Название".PadRight(Book.MaxBookName);
            headers.Add(name);
            string reader = "Читает".PadRight(BookAndReader.MaxReaderName);
            headers.Add(reader);
            string date = "Взял".PadRight(BookAndReader.DateLength);
            headers.Add(date);

            foreach (var header in headers)
            {
                separators.Add(new string('-', header.Length));
            }

            tableHead.Append(CreateTheLineOfElements(headers));
            tableHead.Append(CreateTheLineOfElements(separators));

            return tableHead;
        }

        static StringBuilder CreateTheLineOfElements(List<string> elements)
        {
            var line = new StringBuilder();

            foreach (var element in elements)
            {
                line.Append($"|{element}");
            }
            line.Append("|\n");

            return line;
        }
    }
}