﻿using Newtonsoft.Json;

namespace AdvLab_1_2_3
{
    class ElementOfTheScheme
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; private set; }

        [JsonProperty(PropertyName = "type")]
        public string Type { get; private set; }
    }
}