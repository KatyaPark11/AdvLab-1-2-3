﻿using System;

namespace AdvLab_1_2_3
{
    class DataExamination
    {
        public static void IsAppropriateToSchema(string[] lines, Scheme scheme)
        {
            for (int i = 0; i < lines.Length; i++)
            {
                string[] elements = lines[i].Split(";");
                for (int j = 0; j < elements.Length; j++)
                {
                    string element = elements[j];
                    string type = scheme.Elements[j].Type;
                    bool res = true;

                    switch (type)
                    {
                        case "int":
                            res = Int32.TryParse(element, out _);
                            break;
                        case "bool":
                            res = bool.TryParse(elements[j], out _);
                            break;
                        case "DateTime":
                            res = DateTime.TryParse(elements[j], out _);
                            break;
                    }

                    if (!res) OutputAnErrorMessage(i + 1, j + 1, element, type);
                }
            }
        }

        public static void IsRightCountOfColumns(string[] lines, Scheme scheme)
        {
            for (int i = 0; i < lines.Length; i++)
            {
                string[] elements = lines[i].Split(";");
                int elementsCount = elements.Length;
                int rightCount = scheme.Elements.Count;

                if (elementsCount != rightCount) OutputAnErrorMessage(i + 1, rightCount, elementsCount);
            }
        }

        static void OutputAnErrorMessage(int raw, int column, string element, string type)
        {
            throw new Exception($"There is an error in {raw} raw and {column} column: Cannot convert \"{element}\" to \"{type}\".");
        }

        static void OutputAnErrorMessage(int raw, int rightCount, int wrongCount)
        {
            throw new Exception($"There is an error in {raw} raw:  The count of elements must be {rightCount} but it was {wrongCount}.");
        }
    }
}