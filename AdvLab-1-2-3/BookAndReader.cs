﻿using System;

namespace AdvLab_1_2_3
{
    class BookAndReader
    {
        public static int MaxReaderName = "Читает".Length;
        public static int DateLength = "01.01.0001".Length;

        public readonly Book Book;

        public Reader Reader { get; set; }
        public string DateOfGetting { get; set; }
        public string DateOfReturning { get; set; }

        public BookAndReader(Book book, Reader reader, DateTime dateOfGetting)
        {
            Book = book;
            Reader = reader;
            DateOfGetting = GetTheDate(dateOfGetting);
        }

        public void SetTheDateOfReturning(DateTime dateOfReturning)
        {
            DateOfReturning = GetTheDate(dateOfReturning);
        }

        static string GetTheDate(DateTime date)
        {
            int numOfdaySymbolsInDate = 2, numOfmonthSymbolsInDate = 2, numOfyearSymbolsInDate = 4;

            string day = date.Day.ToString().PadLeft(numOfdaySymbolsInDate, '0');
            string month = date.Month.ToString().PadLeft(numOfmonthSymbolsInDate, '0');
            string year = date.Year.ToString().PadLeft(numOfyearSymbolsInDate, '0');

            return $"{day}.{month}.{year}";
        }
    }
}