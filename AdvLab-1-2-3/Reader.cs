﻿namespace AdvLab_1_2_3
{
    class Reader
    {
        public readonly int Id;
        public readonly string Name;

        public Reader(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}